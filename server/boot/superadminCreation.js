/**
 * Created by admin on 14-09-2016.
 */
var appServer = require('../../server/server');

var Employee = appServer.models.Employee;
var Rolemodel = appServer.models.RoleModel;
var FormList =appServer.models.FormList;
var EmailTemplete=appServer.models.EmailTemplete;
var AppConfig=appServer.models.AppConfig;

var details={
  "firstName": "Super Admin",
  "employeeId": "SuperAdmin00001",
  "lastName": "Super Admin",
  "email": "dmcinfo123@gmail.com",
  "status": "Active",
  "department": "Administration",
  "name": "Super Admin",
  "mobile": "9652774037",
  "workMobile": "9652774037",
  "confirmPassword": "admin@admin",
  "password": "admin@admin",
  "employeeType": "superAdmin",
  "role":"superAdmin",
  "createdPerson": "Admin"
};

var adminDetails=[{
  "firstName": "Scheme Admin",
  "employeeId": "SchemeAdmin00001",
  "lastName": "Scheme Admin",
  "email": "dmcinfoschemes@gmail.com",
  "status": "Active",
  "department": "Administration",
  "name": "Scheme Admin",
  "mobile": "9652774037",
  "workMobile": "9652774037",
  "confirmPassword": "admin@admin",
  "password": "admin@admin",
  "employeeType": "admin",
  "role":"schemeAdmin",
  "createdPerson": "Admin"
},{
  "firstName": "Project Admin",
  "employeeId": "ProjectAdmin00001",
  "lastName": "Project Admin",
  "email": "dmcinfoprojectwardworks@gmail.com",
  "status": "Active",
  "department": "Administration",
  "name": "Project Admin",
  "mobile": "9652774037",
  "workMobile": "9652774037",
  "confirmPassword": "admin@admin",
  "password": "admin@admin",
  "employeeType": "admin",
  "role":"projectAdmin",
  "createdPerson": "Admin"
},{
  "firstName": "Legal Admin",
  "employeeId": "LegalAdmin00001",
  "lastName": "Legal Admin",
  "email": "dmcinfolegalmanagement@gmail.com",
  "status": "Active",
  "department": "Administration",
  "name": "Legal Admin",
  "mobile": "9652774037",
  "workMobile": "9652774037",
  "confirmPassword": "admin@admin",
  "password": "admin@admin",
  "employeeType": "admin",
  "role":"legalAdmin",
  "createdPerson": "Admin"
},{
  "firstName": "Land and Asset Admin",
  "employeeId": "LandAndAssetAdmin00001",
  "lastName": "Land and Asset Admin",
  "email": "dmcinfolandandasset@gmail.com",
  "status": "Active",
  "department": "Administration",
  "name": "Land and Asset Admin",
  "mobile": "9652774037",
  "workMobile": "9652774037",
  "confirmPassword": "admin@admin",
  "password": "admin@admin",
  "role":"landAdmin",
  "employeeType": "admin",
  "createdPerson": "Admin"
}];


var masterDataForForms=[
  {
    "name": "Scheme Management",
    "value": "schemeManagement",
    "typeName": "Scheme",
    "typeValue": "scheme",
    "status": "Active"
  },{
    "name": "Scheme Request",
    "value": "schemeRequests",
    "typeName": "Scheme",
    "typeValue": "scheme",
    "status": "Active"
  },
// Project Ward work Forms
 {
    "name": "Project Charter",
    "value": "projectCharter",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Email",
    "value": "projectWardworksEmailTemplete",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Documents List",
    "value": "documentType",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Department List",
    "value": "planDepartment",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Plan",
    "value": "projectPlan",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
   {
    "name": "Project Task",
    "value": "projectTask",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
  {
    "name": "Project TOR",
    "value": "projectTOR",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
  {
    "name": "Project Issues",
    "value": "projectIssues",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
  {
    "name": "Project RebaseLine",
    "value": "projectRebaseLine",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
  {
    "name": "NOC  Requests",
    "value": "nocRequests",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },

  {
    "name": "Field  Visit",
    "value": "fieldReports",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project  Requests",
    "value": "projectRequests",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
  {
    "name": "Project Measurement Book",
    "value": "projectMeasurementBook",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
  {
    "name": "Project Bill Generation",
    "value": "projectBillGeneration",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
  {
    "name": "Project Payment Order",
    "value": "projectPaymentOrder",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
   {
    "name": "Project Field Visit",
    "value": "projectFieldVisit",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Transfer Funds",
    "value": "projectTransferFunds",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
   {
    "name": "Project Uploads",
    "value": "projectUploads",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Comments",
    "value": "projectComments",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Payment Milestones",
    "value": "projectPaymentMilestones",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Milestones",
    "value": "projectMilestones",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Layout And Uploads",
    "value": "projectLayoutUpload",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Deliverable",
    "value": "projectDeliverable",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Work Order",
    "value": "projectWorkOrder",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Department Wise Budget",
    "value": "projectDepartmentWiseBudget",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Risks",
    "value": "projectRisks",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Closure",
    "value": "projectClosure",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
  // MIS report details

  {
    "name": "Budget Utilization",
    "value": "budgetUtilization",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
{
    "name": "Department Over spent",
    "value": "deptOverspent",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Project Performance",
    "value": "projectPerformance",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },{
    "name": "Weekly Report",
    "value": "weeklyReport",
    "typeName": "Project/Ward Works",
    "typeValue": "project",
    "status": "Active"
  },
// Scheme Master Data

{
    "name": "schemeDepartment",
    "value": "schemeDepartment",
    "typeName": "Configuration Data",
    "typeValue": "configurationData",
    "status": "Active"
  },{
    "name": "Beneficiaries",
    "value": "beneficiaries",
    "typeName": "Configuration Data",
    "typeValue": "configurationData",
    "status": "Active"
  },{
    "name": "Ministries",
    "value": "ministries",
    "typeName": "Configuration Data",
    "typeValue": "configurationData",
    "status": "Active"
  },{
    "name": "Plan State",
    "value": "planState",
    "typeName": "Configuration Data",
    "typeValue": "configurationData",
    "status": "Active"
  }
  ,{
    "name": " Task status",
    "value": "taskStatus",
    "typeName": "Configuration Data",
    "typeValue": "configurationData",
    "status": "Active"
  }
  ,{
    "name": "Head/scheme",
    "value": "headOrScheme",
    "typeName": "Configuration Data",
    "typeValue": "configurationData",
    "status": "Active"
  },{
    "name": "ULB",
    "value": "ulb",
    "typeName": "Configuration Data",
    "typeValue": "configurationData",
    "status": "Active"
  },
  // For User Administration
  {
    "name": "App Configuration",
    "value": "appConfig",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },
  {
    "name": "Employee",
    "value": "employee",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },  {
    "name": "Employee Designation",
    "value": "employeeDesignation",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },
{
    "name": "Employee Type",
    "value": "employeeType",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },{
    "name": "Employee Department",
    "value": "employeeDepartment",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },

  {
    "name": "Contract List",
    "value": "contractorsList",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },
{
    "name": "Email",
    "value": "email",
  "typeName": "Configuration Data",
  "typeValue": "configurationData",
    "status": "Active"
  },{
    "name": "Language Data",
    "value": "languageData",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },{
    "name": "Role",
    "value": "role",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },{
    "name": "Workflow",
    "value": "workflow",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },{
    "name": "Workflow Levels",
    "value": "workflowLevels",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },{
    "name": "Workflow Employee",
    "value": "workflowEmployee",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },{
    "name": "Workflow Forms",
    "value": "workflowForms",
    "typeName": "User Administration",
    "typeValue": "userAdministration",
    "status": "Active"
  },
  // Legal management
  {
    "name": "Case Type",
    "value": "caseType",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Advocate Lists",
    "value": "advocateLists",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  }
  ,{
    "name": "Empannel Group",
    "value": "empannelGroup",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },

  {
    "name": "Status",
    "value": "status",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Priority",
    "value": "priority",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },
  {
    "name": "Acts",
    "value": "acts",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Stake Holders",
    "value": "stakeHolders",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },
  {
    "name": "Court Info",
    "value": "courtInfo",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Case Departments",
    "value": "caseDepartments",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },
  {
    "name": "Payment Mode",
    "value": "paymentMode",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Create Case",
    "value": "createCase",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Advocate Payments",
    "value": "advocatePayments",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Hearing Date Info",
    "value": "hearingDateInfo",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Notice Comments",
    "value": "noticeComments",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },
  {
    "name": "Case Stake Holder",
    "value": "caseStakeHolder",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Doc Upload",
    "value": "docUpload",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Write Petition",
    "value": "writePetition",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Judgement",
    "value": "judgement",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "Case Reports",
    "value": "caseReports",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
    "name": "List of Holidays",
    "value": "holidays",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  }
  ,{
    "name": "Email List",
    "value": "legalEmailTemplate",
    "typeName": "Legal Management",
    "typeValue": "legalManagement",
    "status": "Active"
  },{
     "name": "Account Details",
     "value": "accountDetails",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Type",
     "value": "assetType",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Group",
     "value": "assetGroup",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Profile",
     "value": "assetProfile",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Criticality",
     "value": "assetCriticality",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Maintenance Objectives",
     "value": "maintenanceObjectives",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Type Of Duty",
     "value": "typeOfDuty",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Failure Classes",
     "value": "failureClasses",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Spares And Consumables",
     "value": "assetSparesAndConsumables",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Document List Master",
     "value": "documentListMaster",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Status",
     "value": "assetStatus",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset KPIs",
     "value": "assetKPIs",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Service Level",
     "value": "assetServiceLevel",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Disposal Methods",
     "value": "assetDisposalMethods",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Risks",
     "value": "assetRisks",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Cost Center",
     "value": "costCenter",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Policies",
     "value": "assetPolicies",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Life Cycle Plan",
     "value": "assetLifeCyclePlan",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Department",
     "value": "assetDepartment",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Email Configuration",
     "value": "assetEmailConfiguration",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Create Asset Land",
     "value": "createAssetLand",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Create Asset Others",
     "value": "createAssetOthers",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Cost Transfer Funds",
     "value": "costTransferFunds",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Create Asset Objectives And KPIs",
     "value": "createAssetObjectivesAndKPIs",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
    },{
     "name": "Create Asset Spares And Consumables",
     "value": "createAssetSparesAndConsumables",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Create Asset Failure And Maintenance",
     "value": "createAssetFailureAndMaintenance",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Create Asset Risks",
     "value": "createAssetRisks",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Create Asset Causes",
     "value": "createAssetCauses",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Create Asset Service",
     "value": "createAssetServiceLevelForecast",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Asset Demand Projection",
     "value": "assetDemandProjection",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Purchase Requests",
     "value": "purchaseRequests",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Purchase Requests Report",
     "value": "purchaseRequestsReport",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Purchase Agenda",
     "value": "purchaseAgenda",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Assign Driver",
     "value": "assignDriver",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Log Book",
     "value": "logBook",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   },{
     "name": "Submit Bills",
     "value": "submitBills",
     "typeName": "Land And Asset Management",
     "typeValue": "landAndAssetManagement",
     "status": "Active"
   }
]

if(masterDataForForms!=undefined && masterDataForForms!=null && masterDataForForms.length>0){
  for(var i=0;i<masterDataForForms.length;i++){
    FormList.findOrCreate({"where":{"value":masterDataForForms[i].value}},masterDataForForms[i], function(err, output){
    });
  }
}


Employee.findOrCreate({"where":{ "employeeType": "superAdmin"}},details, function(err, output){
  if(err){
    console.log('Error creating Super Admin Role ' + err);
    throw err;
  } else {
  }
});

Employee.findOrCreate({"where":{ "employeeType": "admin"}},adminDetails, function(err, output){
  if(err){
    console.log('Error creating Super Admin Role ' + err);
    throw err;
  }
});




/*
var emailCreate={
  "emailId":"dmcinfo123@gmail.com",
  "hearingSubject":"",
  "hearingSMS":"",
  "hearingText":"",
  "caseSubject":"",
  "caseSMS":"",
  "caseText":"",
  "emailType":"legal"
};

EmailTemplete.findOrCreate({"where":{"emailType":"legal"}},emailCreate,function(err, emailTemplete){

  if(err){
    console.log('err in email template'+err);
  }
});*/



var emailCreate={
  "emailId":"dmcinfo123@gmail.com",
  "registerSubject":"",
  "citizenSubject":"",
  "requestEmail":"",
  "levelEmail":"",
  "requestApprovalEmail":"",
  "rejectedEmail":"",
  "adminSubject":"",
  "adminSMS":"",
  "rejectedSMS":"",
  "requestApprovalSMS":"",
  "levelSMS":"",
  "requestSMS":"",
  "citizenSMS":"",
  "registerSMS":"",
  "registerText":"",
  "citizenText":"",
  "requestText":"",
  "levelText":"",
  "requestApprovalMessage":"",
  "rejectedMessage":"",
  "adminText":"",
  "emailType":"scheme"
};
EmailTemplete.findOrCreate({"where":{"emailType":"scheme"}},emailCreate,function(err, emailTemplete){

  if(err){
    console.log('err in email template'+err);
  }
});

/*

 emailCreate={
  "emailId":"dmcinfo123@gmail.com",
  "billGenerationRejectedEmail": "",
  "billGenerationRejectedMessage": "",
  "billGenerationRejectedSMS": "",
  "billGenerationRequestApprovalEmail": "",
  "billGenerationRequestApprovalMessage": "",
  "billGenerationRequestApprovalSMS": "",
  "billGenerationRequestEmail": "",
  "billGenerationRequestSMS": "",
  "billGenerationRequestText": "",
  "budgetRejectedEmail": "",
  "budgetRejectedMessage": "",
  "budgetRejectedSMS": "",
  "budgetRequestApprovalEmail": "",
  "budgetRequestApprovalMessage": "",
  "budgetRequestApprovalSMS": "",
  "budgetRequestEmail": "",
  "budgetRequestSMS": "",
  "budgetRequestText": "",
  "docUploadRejectedEmail": "",
  "docUploadRejectedMessage": "",
  "docUploadRejectedSMS": "",
  "docUploadRequestApprovalEmail": "",
  "docUploadRequestApprovalMessage": "",
  "docUploadRequestApprovalSMS": "",
  "docUploadRequestEmail": "",
  "docUploadRequestSMS": "",
  "docUploadRequestText": "",
  "fieldVisitRejectedEmail": "",
  "fieldVisitRejectedMessage": "",
  "fieldVisitRejectedSMS": "",
  "fieldVisitRequestApprovalEmail": "",
  "fieldVisitRequestApprovalMessage": "",
  "fieldVisitRequestApprovalSMS": "",
  "fieldVisitRequestEmail": "",
  "fieldVisitRequestSMS": "",
  "fieldVisitRequestText": "",
  "lastEditPerson": "Admin",
  "transferRejectedEmail": "",
  "transferRejectedMessage": "",
  "transferRejectedSMS": "",
  "transferRequestApprovalEmail": "",
  "transferRequestApprovalMessage": "",
  "transferRequestApprovalSMS": "",
  "transferRequestEmail": "",
  "transferRequestSMS": "",
  "transferRequestText": "",
  "emailType":"projectWardWorks"

};
EmailTemplete.findOrCreate({"where":{"emailType":"projectWardWorks"}},emailCreate,function(err, emailTemplete){

  if(err){
    console.log('err in email template'+err);
  }

});


//land and asset email template start

 emailCreate={
  "emailId":"dmcinfo123@gmail.com",
  "demandProjectionRequestEmail":"",
  "demandProjectionRequestSMS":"",
  "demandProjectionRequestText":"",
  "demandProjectionApprovalEmail":"",
  "demandProjectionApprovalSMS":"",
  "demandProjectionApprovalMessage":"",
  "demandProjectionRejectedEmail":"",
  "demandProjectionRejectedSMS":"",
  "demandProjectionRejectedMessage":"",
  "purchaseAgendaRequestEmail":"",
  "purchaseAgendaRequestSMS":"",
  "purchaseAgendaRequestText":"",
  "purchaseAgendaApprovalEmail":"",
  "purchaseAgendaApprovalSMS":"",
  "purchaseAgendaApprovalMessage":"",
  "purchaseAgendaRejectedEmail":"",
  "purchaseAgendaRejectedSMS":"",
  "purchaseAgendaRejectedMessage":"",
  "billSubmitRequestEmail":"",
  "billSubmitRequestSMS":"",
  "billSubmitRequestText":"",
  "billSubmitApprovalEmail":"",
  "billSubmitApprovalSMS":"",
  "billSubmitApprovalMessage":"",
  "billSubmitRejectedEmail":"",
  "billSubmitRejectedSMS":"",
  "billSubmitRejectedMessage":"",
  "emailType":"landAndAsset"

};
EmailTemplete.findOrCreate({"where":{"emailType":"landAndAsset"}},emailCreate,function(err, emailTemplete){

  if(err){
    console.log('err in email template'+err);
  }

});

*/


var appConfig={
  "financialYear": "2016-2017",
  "currentFinancialYear": false,
  "nocAmountAction": 0,
  "nocAmount": false,
  "escalationDays": 0,
  "autoEscalation": false,
  "emailText": "",
  "emailTextData": false,
  "type": "appConfig"
};

AppConfig.findOrCreate({"where":{"type":"appConfig"}},appConfig,function(err, appConfig){

  if(err){
    console.log('err in app config'+err);
  }

});
