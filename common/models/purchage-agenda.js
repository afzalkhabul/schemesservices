var server = require('../../server/server');

module.exports = function(PurchageAgenda) {

  PurchageAgenda.observe('before save', function (ctx, next) {

    if(ctx.instance!=undefined && ctx.instance!=null){
      ctx.instance.createdTime = new Date();

      var randomString = require("randomstring");

      var meetingDate = ctx.instance.meetingDate.split('-').join('/');

      var department =ctx.instance.departmentName.toUpperCase();

      var purchaseAgendaNumber = randomString.generate({
        length: 4,
        charset: 'alphanumeric'
      });

      ctx.instance['purchaseAgendaNumber'] = meetingDate+"/"+department.substring(0,3)+"/"+purchaseAgendaNumber.toUpperCase();
      next();
    } else {

      ctx.data.updatedTime = new Date();
      next();

    }
  });
};

