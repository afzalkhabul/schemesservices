var server = require('../../server/server');
module.exports = function(Workflowemployees) {
  Workflowemployees.observe('before save', function (ctx, next) {


    if(ctx.instance){
      ctx.instance.createdTime=new Date();
      next();

    }else{
      ctx.data.updateTime=new Date();
      next();
    }
  });
  Workflowemployees.getEmployee = function (filterData, cb) {
    console.log('input Data'+JSON.stringify(filterData));
    if(filterData.workflowId!=undefined && filterData.workflowId!=null &&
      filterData.workflowLevel!=undefined && filterData.workflowLevel!=null ){
      Workflowemployees.find({"where":{"and":[{"workflowId": filterData.workflowId},{"levelId": filterData.workflowLevel}]}}, function (err, employeeList) {
        if(employeeList!=null && employeeList.length>0){
          var Employee=server.models.Employee;
          var employeesData=[];
          for(var x=0;x<employeeList.length;x++){
            if(employeeList[x].employees!=undefined && employeeList[x].employees.length>0){
              for(var i=0;i<employeeList[x].employees.length;i++){
                employeesData.push({'employeeId':employeeList[x].employees[i]});
              }
            }
          }
          //console.log('employess data '+employeesData);
          //console.log('employess data qurey  {"where":{"employeeId":"'+employeesData+'"}}');
          Employee.find({'where':{'or':employeesData}}, function (err, employeeData) {
            console.log('data'+employeeData.length);
            cb(null, employeeData);
          })
        }
      })
    }

  };


  Workflowemployees.remoteMethod('getEmployee', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root: true
    },
    accepts: [{arg: 'filter', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/getEmployee',
      verb: 'POST'
    }
  });


};
