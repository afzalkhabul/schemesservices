var server = require('../../server/server');
module.exports = function(Judgement) {
  Judgement.validatesUniquenessOf('judgementId', {message: 'Judgement Id could be unique'});
  Judgement.observe('before save', function (ctx, next) {
    if(ctx.instance!=undefined && ctx.instance!=null){
      ctx.instance.createdTime = new Date();
      // var randomstring = require("randomstring");
      // var judgementId = randomstring.generate({
      //   length: 4,
      //   charset: 'numeric'
      // });
      var judgementId = Math.floor(Math.random() * Math.floor(10000));
      ctx.instance['judgementId'] = judgementId;
            var judgementCheckDate = ctx.instance.judgementDate;
            console.log("----------------------------"+JSON.stringify(judgementCheckDate))
            var holidayList = server.models.Holiday;
            var createCase = server.models.CreateCase;
            var hearingdate = server.models.HearingDate;
            console.log("00000000000:::::::                         ")
            holidayList.find({"where":{"festivalDate":judgementCheckDate}},function (err, Holiday) {
            if(Holiday.length>0){
             var error = new Error("We have holiday for this Judgement date");
              error.statusCode = 200;
              next(error);
            }else{
            next()
            }
            });
      }else {
      ctx.data.updatedTime = new Date();
      next();
    }
  });

  Judgement.observe('after save', function (ctx, next) {
    if(ctx.isNewInstance){
      var judgeStatus = ctx.instance.status;
      var judgeCourt = ctx.instance.court;
      var createCase = server.models.CreateCase;
      var hearingDate = server.models. HearingDate
      createCase.find({"where":{"caseNumber":ctx.instance.caseNumber}},function (err, CreateCase) {
        CreateCase[0].updateAttributes({status : judgeStatus,courtName:judgeCourt},function(err,sucuss){
        })
        hearingDate.find({"where":{"caseNumber":ctx.instance.caseNumber},
        order:'editDate DESC'},function (err, HearingDate) {

        if(HearingDate.length>0) {
            HearingDate[0].updateAttributes({status: judgeStatus}, function (err, sucuss) {
            })
          }
          next();
        });
      });
    }else {
      next();
    }
  });
};
