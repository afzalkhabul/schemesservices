var server = require('../../server/server');
module.exports = function(Rolemodel) {


  Rolemodel.validatesUniquenessOf('name', {message: 'Name could be unique'});

  Rolemodel.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.name=(ctx.instance.name.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();

    } else {
      if(ctx.data.name!=undefined && ctx.data.name!=null && ctx.data.name!=''){
        ctx.data.name=(ctx.data.name.toLowerCase());
      }

      ctx.data.updatedTime = new Date();
      next();



    }
  });
  Rolemodel.getRoleStatus = function (employeeId,formName, cb) {
      var RoleEmployees=server.models.RoleEmployees;
      var Employee=server.models.Employee;
    Employee.find({'where':{'employeeId':employeeId}}, function (err, employeeDetails) {
      if(employeeDetails!=undefined && employeeDetails!=null && employeeDetails.length>0){
       var employeeData=employeeDetails[0];
        if(employeeData.role=='superAdmin'){
          cb(null,true);
        }
        else if(employeeData.role=='schemeAdmin'){
          if(formName!=undefined && formName!=null && formName!=''){
            if (formName == "schemeManagement" || formName == "schemeRequests" || formName== "ministries" || formName== "schemeDepartment"
            || formName == "beneficiaries" || formName== "emailAccess" ) {
              cb(null,true);
            }
            else if (formName=="workflowForms" || formName== "workflowEmployee" ||formName== "workflowLevels" || formName== "workflow" ||
              formName== "role" || formName== "languageData" || formName== "employeeType" || formName== "employeeDepartment"|| formName== "employeeDesignation" || formName== "employee") {
              cb(null,true);
            }else{
              cb(null,false);
            }
          }else{
            cb(null,false);
          }
        }
        else if(employeeData.role=='projectAdmin'){
         if (formName== "headOrScheme" || formName== "ulb"|| formName== "planState" ||formName== "taskStatus" || formName== "projectWardworksEmailTemplete" ||
           formName== "documentType" ||formName== "planDepartment" || formName== "projectCharter" || formName== "nocRequests" || formName== "fieldReports" ||
          formName== "projectRequests"|| formName== "projectPlan"|| formName== "projectTask" ||formName== "projectTOR" || formName== "projectIssues" || formName== "projectRebaseLine"
        || formName== "projectLayoutUpload" ||formName== "projectBillGeneration" || formName== "projectFieldVisit" || formName== "projectTransferFounds" ||
          formName== "projectUploads" || formName== "projectComments"|| formName== "projectDeliverable" || formName== "projectPaymentMilestones" || formName== "projectMilestones"
         ||formName== "projectDepartmentWiseBudget" || formName== "projectRisks" || formName== "projectClosure") {
           cb(null,true);
          }
         else if (formName=="workflowForms" || formName== "workflowEmployee" ||formName== "workflowLevels" || formName== "workflow" ||
           formName== "role" ||formName== "languageData" || formName== "employeeType" || formName== "employeeDepartment" || formName== "employeeDesignation" || formName== "employee") {
           cb(null,true);
         }else{
           cb(null,false);
         }
        }
        else if(employeeData.role=='legalAdmin'){
          if (formName== "holidays" || formName== "empannelGroup"|| formName== "advocateLists" ||formName== "status" || formName== "priority" ||
            formName== "acts" ||formName== "stakeHolders" || formName== "courtInfo" || formName== "caseDepartments" || formName== "paymentMode" ||
            formName== "legalEmailTemplate"|| formName== "caseType"|| formName== "createCase" ||formName== "advocatePayments" || formName== "hearingDateInfo" || formName== "noticeComments"
            || formName== "docUpload" ||formName== "writePetition" || formName== "judgement" || formName== "caseReports" ) {
            cb(null,true);
          }
        else if (formName=="workflowForms" || formName== "workflowEmployee" ||formName== "workflowLevels" || formName== "workflow" ||
          formName== "role" || formName== "languageData" ||formName== "employeeType" || formName== "employeeDepartment" || formName== "employeeDesignation" || formName== "employee") {
          cb(null,true);
        }else{
          cb(null,false);
        }
        }
        else if(employeeData.role=='landAdmin'){
          if (formName== "submitBills" || formName== "logBook"|| formName== "assignDriver" ||formName== "purchaseAgenda" || formName== "purchaseRequestsReport" ||
            formName== "purchaseRequests" ||formName== "assetDemandProjection" || formName== "createAssetRisks" || formName== "createAssetCauses" || formName== "createAssetServiceLevelForecast" ||
            formName== "createAssetFailureAndMaintenance"|| formName== "createAssetSparesAndConsumables"|| formName== "createAssetObjectivesAndKPIs" ||formName== "costTransferFunds" || formName== "createAssetOthers" || formName== "createAssetLand"
            || formName== "assetEmailConfiguration" ||formName== "assetLifeCyclePlan" || formName== "assetDepartment" || formName== "assetPolicies" ||
            formName== "costCenter" || formName== "assetRisks"|| formName== "assetDisposalMethods" || formName== "assetDisposalMethods" || formName== "accountDetails"
            ||formName== "assetKPIs" || formName== "assetStatus" || formName== "documentListMaster"||
            formName== "assetSparesAndConsumables" ||formName== "failureClasses" || formName== "typeOfDuty" || formName== "maintenanceObjectives" ||
            formName== "assetCriticality" || formName== "assetProfile"|| formName== "assetGroup" || formName== "assetType"
          ) {
            cb(null,true);
          }
          else if (formName=="workflowForms" || formName== "workflowEmployee" ||formName== "workflowLevels" || formName== "workflow" ||
            formName== "role" || formName== "languageData" ||formName== "employeeType" || formName== "employeeDepartment" || formName== "employeeDesignation" || formName== "employee") {
            cb(null,true);
          }else{
            cb(null,false);
          }
        }
        else{
          RoleEmployees.find({"where": {"status": "Active"}}, function (err, roleModelList) {

            if (roleModelList != null && roleModelList.length > 0) {
              var roleList = [];
              for (var i = 0; i < roleModelList.length; i++) {
                if (roleModelList[i].formDetails != undefined && roleModelList[i].formDetails != null && roleModelList[i].formDetails.length > 0) {
                  for (var j = 0; j < roleModelList[i].formDetails.length; j++) {
                    if (roleModelList[i].formDetails[j].view != undefined && roleModelList[i].formDetails[j].view) {
                      if (roleModelList[i].formDetails[j].employeeId == employeeId) {
                        var object = {
                          'id': roleModelList[i].roleId
                        }
                        roleList.push(object);
                      }
                    }

                  }
                }
              }
              if(roleList!=undefined && roleList!=null && roleList.length>0){
                Rolemodel.find({'where': {'or': roleList}}, function (err, roleFormDetails) {
                  if (roleFormDetails != null && roleFormDetails.length > 0) {
                    checkAccessDetailsDetails(roleFormDetails, formName,cb);
                  } else {
                    cb(null,false);
                  }

                });

              }else{
                cb(null,false);
              }
            } else {
              cb(null,false);
            }
          });
        }
      }else{
        cb(null,false);
      }
    });
  }
function checkAccessDetailsDetails(roleForms,formName,cb){
  if(roleForms.length>0){
      var status=false;
    for(var i=0;i<roleForms.length;i++){
      if(roleForms[i].formDetails!=undefined && roleForms[i].formDetails!=null && roleForms[i].formDetails.length>0){
        for(var j=0;j<roleForms[i].formDetails.length;j++){
          var formDetails=roleForms[i].formDetails[j];
          if (formDetails.value != undefined && formDetails.value !=null && formDetails.value ==formName &&( formDetails.view || formDetails.edit) ) {
            status=true;
            break;
          }
        }
        if(status){
          break;
        }
      }
    }
      cb(null,status);
  }else{
   cb(null,false);
  }

}

  Rolemodel.remoteMethod('getRoleStatus',{
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true},{arg: 'formName', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getRoleStatus',
      verb: 'GET'
    }
  });







};
