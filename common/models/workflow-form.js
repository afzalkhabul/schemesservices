var server = require('../../server/server');
module.exports = function(Workflowform) {
  Workflowform.observe('before save', function (ctx, next) {

    var data;
    if(ctx.instance){
      data = ctx.instance;
    }else{
      data = ctx.data;
    }
    Workflowform.find({"where":{"schemeUniqueId":data.schemeUniqueId}},function(err, formData){
      if(err){
        console.log('getting error here');
        var error = new Error('This form already having work flow');
        error.statusCode = 200;
        next(error, null);
      }else{
        if(formData.length > 0){
          if(ctx.instance) {
            var error = new Error('This form already having work flow');
            error.statusCode = 200;
            next(error, null);
          }else{
            if(data.id == formData[0].id){
              next();
            }else{
              var error = new Error('This form already having work flow');
              error.statusCode = 200;
              next(error, null);
            }
          }
        }else{
          next();
        }
      }
    });
    //next();
  });
};
