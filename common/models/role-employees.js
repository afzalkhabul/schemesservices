var server = require('../../server/server');
module.exports = function(Roleemployees) {
  Roleemployees.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      if(ctx.instance.roleId!=undefined && ctx.instance.roleId!=null && ctx.instance.roleId!='' ){

        var RoleModel=server.models.RoleModel;
        RoleModel.findById(ctx.instance.roleId, function (err, roleDetails) {
          if(roleDetails!=null && roleDetails.name ){
            ctx.instance.name=roleDetails.name;
            next();
          }else{
            next();
          }
        });
      }else{
        next();
      }
    }else{
      next();
    }
  });
};
