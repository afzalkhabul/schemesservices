var server = require('../../server/server');
module.exports = function(AssetForLand) {


    AssetForLand.observe('before save', function (ctx, next) {

    if(ctx.instance!=undefined && ctx.instance!=null){
      ctx.instance.createdTime = new Date();
      var randomstring = require("randomstring");
      var landType = ctx.instance.assetType.toUpperCase();
       var locality =ctx.instance.locality.toUpperCase();
      var assetNumber = randomstring.generate({
        length: 6,
        charset: 'alphanumeric'
      });
      ctx.instance['assetNumber'] = landType.substring(0,4)+"_"+locality.substring(0,3)+"_"+assetNumber.toUpperCase();
    if(ctx.instance.projectDetails!=undefined && ctx.instance.projectDetails
        && ctx.instance.projectDetails.projectCharter!=undefined && ctx.instance.projectDetails.projectCharter && ctx.instance.projectDetails.projectCharter!='' ){

        var Projectcharter = server.models.ProjectCharter;
        Projectcharter.findOne({'where':{'charterUniqueId':ctx.instance.projectDetails.projectCharter}}, function (err, charterDetails) {
          if(charterDetails!=null){
            var charter={
              'name':charterDetails.name,
              'description':charterDetails.description,
              'createdPerson':charterDetails.createdPerson,
              'status':'new',
              'charterUniqueId':charterDetails.charterUniqueId
            };
            Projectcharter.create(charter, function (err, charterData) {
              if(charterData!=null){
                var TaskStatus=server.models.TaskStatus;
                var ProjectPlan=server.models.ProjectPlan;
                var details={};
                TaskStatus.findOne({'where':{'name':'new'}},function(err, taskDetails){
                 if(taskDetails!=null){
                   details={
                     "planId": charterData.projectId,
                     "name": ctx.instance.projectDetails.projectPlan,
                     "departmentId": ctx.instance.projectDetails.departmentInfo,
                     "financialYear": ctx.instance.projectDetails.financialYear,
                     'taskStatusId':taskDetails.id
                   }
                   ProjectPlan.create(details, function (err, planDetails) {
                     if(planDetails!=null){
                       ctx.instance['planId']=planDetails.planId;
                       next();
                     }else{
                       next();
                     }
                   });
                 }else{
                   details={
                     "planId": charterData.projectId,
                     "name": ctx.instance.projectDetails.projectPlan,
                     "departmentId": ctx.instance.projectDetails.departmentInfo,
                     "financialYear": ctx.instance.projectDetails.financialYear,
                   }
                   ProjectPlan.create(details, function (err, planDetails) {
                     if(planDetails!=null){
                       ctx.instance['planId']=planDetails.planId;
                       next();
                     }else{
                       next();
                     }
                   });
                 }

                });
              }else{
                next();
              }

            });

          }else{
            next();
          }

        });


      }
      else{
        next();
      }
    }else {

      ctx.data.updatedTime = new Date();
      next();

    }
  });

    AssetForLand.observe('loaded', function(ctx, next) {

     if(ctx.instance) {
          if(ctx.instance.department!=undefined && ctx.instance.department!=null && ctx.instance.department!='' ){
          var department = server.models.AssetDepartments;
          var deptId=ctx.instance.department;
          var contactPersonDepartmentId=ctx.instance.contactPersonDepartment;
          var maintenanceContactDepartmentId=ctx.instance.maintenanceContactDepartment;
          department.findById(deptId,function(err,dept){
          if(dept!=undefined && dept){
          var departmentValue=dept.name;
                 ctx.instance['departmentValue'] = departmentValue;
                  department.findById(contactPersonDepartmentId,function(err,contactPersonDept)
                          {
                             if(contactPersonDept!=undefined && contactPersonDept)
                             {
                               var contactDeptValue=contactPersonDept.name;
                               ctx.instance['contactDeptValue']=contactDeptValue;
                             }
                              department.findById(maintenanceContactDepartmentId,function(err,maintenanceContactDept)
                                         {
                                            if(maintenanceContactDept!=undefined && maintenanceContactDept)
                                            {
                                              var maintenanceContactDepartmentValue=maintenanceContactDept.name;
                                              ctx.instance['maintenanceContactDepartmentValue']=maintenanceContactDepartmentValue;
                                            }
                                             next();
                                         });
                          });
          }else{
          next();
          }
        });
          }else{
          next();
          }
        }
            else{
              next();
            }
          });

};


